require 'rubygems'
require 'net/https'
require 'uri'
require 'spreadsheet'
Spreadsheet.client_encoding = 'UTF-8'

class LinkChecker

  def self.send_request_and_get_request(url)
    url = URI.escape(url)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    @response = http.request(request)
  end

  def self.worksheet(file_location, index_of_workingsheet)
    file = Spreadsheet.open file_location
    return file.worksheet(index_of_workingsheet)
  end

  def self.get_the_from_value(worksheet)
    from_array = []
    worksheet.count.times do |i|
      row = worksheet.row(i)
      from_array << row[0]
    end
    return from_array
  end

  def self.get_the_to_value(worksheet)
    to_array = []
    worksheet.count.times do |i|
      row = worksheet.row(i)
      to_array << row[1]
    end
    return to_array
  end


  def LinkChecker.verify_response_code_and_location_are_correct(path_to_xml_file, index_of_worksheet)
    worksheet = worksheet(path_to_xml_file, index_of_worksheet)
    from_value = get_the_from_value(worksheet)
    to_value = get_the_to_value(worksheet)
    worksheet.count.times do |i|
      response = send_request_and_get_request(from_value[i])
      if response.code != "301" && response.code != "302"
        puts from_value[i]+" No redirect at all"
      else
        begin
          if URI.unescape(response["location"]).include? URI.unescape(to_value[i]).to_s.downcase.gsub('http://','')
          else
            puts from_value[i]+" Following url doesn't redirect to url which adjusted in file"
          end
        rescue
          next
        end
      end
    end
  end
end